﻿namespace LibraryBooks.Model
{
    using System;
    using System.Collections.Generic;

    using LibraryBooks.Model.BusinessObjects;

    public interface IModel
    {
        IList<BookModel> GetBooks(string sortExpression);

        BookModel GetBook(string isbn);

        void UpdateBook(BookModel book);

        void AddBook(BookModel book);

        void DeleteBook(string isbn);

        IList<BookModel> SearchBooks(BookModel book);

    }
}