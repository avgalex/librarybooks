﻿namespace LibraryBooks.Model
{
    using System.Collections.Generic;
    using System.Linq;
    using LibraryBooks.Model.BusinessObjects;
    using LibraryBooks.Model.ServiceReference1;

    /// <summary>
    /// Static class that maps data transfer objects to model objects and vice versa.
    /// </summary>
    internal static class Mapper
    {
        internal static IList<BookModel> FromDataTransferObjects(Book[] books)
        {
            if (books == null)
            {
                return null;
            }

            return books.Select(c => FromDataTransferObject(c)).ToList();
        }
        
        internal static BookModel FromDataTransferObject(Book book)
        {
            return new BookModel()
                       {
                           Author = book.Author,
                           DatePublish = book.DatePublish,
                           DatePurchase = book.DatePurchase,
                           ISBN = book.ISBN,
                           PageNumber = book.PageNumber,
                           Title = book.Title
                       };
        }
        
        internal static Book ToDataTransferObject(BookModel bookModel)
        {
            return new Book()
                       {
                          Author = bookModel.Author,
                          DatePublish = bookModel.DatePublish.Value,
                          DatePurchase = bookModel.DatePurchase.Value,
                          ISBN = bookModel.ISBN,
                          PageNumber = bookModel.PageNumber,
                          Title = bookModel.Title
                       };
        }
    }
}
