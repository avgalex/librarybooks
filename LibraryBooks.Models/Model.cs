﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Model.cs" company="">
//   
// </copyright>
// <summary>
//   The model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LibraryBooks.Model
{
    using System;
    using System.Collections.Generic;

    using LibraryBooks.Model.BusinessObjects;
    using LibraryBooks.Model.ServiceReference1;

    using NLog;

    /// <summary>
    /// The model.
    /// </summary>
    public class Model : IModel
    {
        #region Static Fields

        /// <summary>
        /// The logger.
        /// </summary>
        private static Logger logger = LogManager.GetLogger(" Model");

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes static members of the <see cref="Model"/> class.
        /// </summary>
        static Model()
        {
            try
            {
                Service = new LibraryBooksServiceClient();
            }
            catch (Exception e)
            {
                logger.Error("Error initialize service", e);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the service.
        /// </summary>
        private static ILibraryBooksService Service { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The add book.
        /// </summary>
        /// <param name="book">
        /// The book.
        /// </param>
        public void AddBook(BookModel book)
        {
            var request = PrepareRequest(new BookRequest());
            request.Action = "Create";
            request.Book = Mapper.ToDataTransferObject(book);

            try
            {
                Service.SetBooks(request);
            }
            catch (Exception e)
            {
                logger.Error("Error add book", e);
            }
        }

        /// <summary>
        /// The delete book.
        /// </summary>
        /// <param name="isbn">
        /// The isbn.
        /// </param>
        public void DeleteBook(string isbn)
        {
            var request = PrepareRequest(new BookRequest());
            request.Action = "Delete";
            request.Criteria = new BookCriteria() { ISBN = isbn };
            try
            {
                Service.SetBooks(request);
            }
            catch (Exception e)
            {
                logger.Error("Error delete book", e);
            }
        }

        public IList<BookModel> SearchBooks(BookModel book)
        {
            var request = PrepareRequest(new BookRequest());

            request.LoadOptions = new[] { "Search" };
            request.Criteria = new BookCriteria()
                                   {
                                       ISBN = book.ISBN,
                                       Author = book.Author,
                                       DatePublish = book.DatePublish,
                                       DatePurchase = book.DatePurchase,
                                   PageNumber = book.PageNumber,
                                   Title = book.Title
                                   };

            var response = Service.GetBooks(request);

            if (response.CorrelationId != request.RequestId)
            {
                throw new ApplicationException("GetBooks: RequestId and CorrelationId do not match.");
            }

            if (response.Acknowledge != AcknowledgeType.Success)
            {
                throw new ApplicationException(response.Message);
            }

            var books = Mapper.FromDataTransferObjects(response.Books);
           
            return books;
        }

      

        /// <summary>
        /// The get book.
        /// </summary>
        /// <param name="isbn">
        /// The isbn.
        /// </param>
        /// <returns>
        /// The <see cref="BookModel"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// </exception>
        public BookModel GetBook(string isbn)
        {
            var request = PrepareRequest(new BookRequest());

            request.LoadOptions = new[] { "Book" };
            request.Criteria = new BookCriteria() { ISBN = isbn };

            var response = Service.GetBooks(request);

            if (response.CorrelationId != request.RequestId)
            {
                throw new ApplicationException("GetBooks: RequestId and CorrelationId do not match.");
            }

            if (response.Acknowledge != AcknowledgeType.Success)
            {
                throw new ApplicationException(response.Message);
            }

            return Mapper.FromDataTransferObject(response.Book);
        }

        /// <summary>
        /// The get books.
        /// </summary>
        /// <param name="sortExpression">
        /// The sort expression.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        /// <exception cref="ApplicationException">
        /// </exception>
        public IList<BookModel> GetBooks(string sortExpression)
        {
            var request = PrepareRequest(new BookRequest());

            request.LoadOptions = new[] { "Books" };
            request.Criteria = new BookCriteria() { SortExpression = sortExpression };

            var response = Service.GetBooks(request);

            if (response.CorrelationId != request.RequestId)
            {
                throw new ApplicationException("GetBooks: RequestId and CorrelationId do not match.");
            }

            if (response.Acknowledge != AcknowledgeType.Success)
            {
                throw new ApplicationException(response.Message);
            }

            return Mapper.FromDataTransferObjects(response.Books);
        }

        /// <summary>
        /// The update book.
        /// </summary>
        /// <param name="book">
        /// The book.
        /// </param>
        public void UpdateBook(BookModel book)
        {
            var request = PrepareRequest(new BookRequest());
            request.Action = "Update";
            request.Book = Mapper.ToDataTransferObject(book);

            try
            {
                Service.SetBooks(request);
            }
            catch (Exception e)
            {
                logger.Error("Error update book", e);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The prepare request.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <typeparam name="T">
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        private static T PrepareRequest<T>(T request) where T : RequestBase
        {
            request.RequestId = Guid.NewGuid().ToString();

            return request;
        }

        #endregion
    }
}