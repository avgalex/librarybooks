﻿namespace LibraryBooks.Model.BusinessObjects
{
    using System;

    public class BookModel
    {
        public string ISBN { get; set; }

        public string Title { get; set; }

        public DateTime? DatePublish { get; set; }

        public DateTime? DatePurchase { get; set; }

        public int PageNumber { get; set; }

        public string Author { get; set; }
       
    }
}