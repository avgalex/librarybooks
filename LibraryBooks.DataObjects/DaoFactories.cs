namespace LibraryBooks.DataObjects
{
    public class DaoFactories
    {
        public static IDaoFactory GetFactory(string dataProvider)
        {
            // Return the requested DaoFactory
            switch (dataProvider)
            {
                case "ADO.NET.SqlExpress":
                case "ADO.NET.SqlServer": return new SqlServer.SqlServerDaoFactory();
                
                // Default: SqlExpress
                default: return new SqlServer.SqlServerDaoFactory(); 
            }
        }
    }
}
