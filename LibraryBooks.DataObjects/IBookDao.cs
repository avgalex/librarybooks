namespace LibraryBooks.DataObjects
{
    using System;
    using System.Collections.Generic;

    using BusinessObjects;

    public interface IBookDao
    {
        #region Public Methods and Operators

        /// <summary>
        /// Deletes a customer
        /// </summary>
        /// <param name="customer">
        /// Customer.
        /// </param>
        void DeleteBook(Book book);

        /// <summary>
        /// The get book.
        /// </summary>
        /// <param name="Isbn">
        /// The isbn.
        /// </param>
        /// <returns>
        /// The <see cref="Book"/>.
        /// </returns>
        Book GetBook(string Isbn);

        /// <summary>
        /// The get books.
        /// </summary>
        /// <param name="sortExpression">
        /// The sort expression.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Book> GetBooks(string sortExpression = "ISBN ASC");

        /// <summary>
        /// Inserts a new customer. 
        /// </summary>
        /// <remarks>
        /// Following insert, customer object will contain the new identifier.
        /// </remarks>
        /// <param name="customer">
        /// Customer.
        /// </param>
        void InsertBook(Book book);

        /// <summary>
        /// The search products.
        /// </summary>
        /// <param name="isbn">
        /// The isbn.
        /// </param>
        /// <param name="title">
        /// The title.
        /// </param>
        /// <param name="datePublish">
        /// The date publish.
        /// </param>
        /// <param name="datePurchase">
        /// The date purchase.
        /// </param>
        /// <param name="Title">
        /// The title.
        /// </param>
        /// <param name="author">
        /// The author.
        /// </param>
        /// <param name="pageNumber">
        /// The page number.
        /// </param>
        /// <param name="sortExpression">
        /// The sort expression.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<Book> SearchBooks(
            string isbn, 
            string title, 
            DateTime? datePublish, 
            DateTime? datePurchase, 
            string Title, 
            string author, 
            int pageNumber, 
            string sortExpression);

        /// <summary>
        /// Updates a customer.
        /// </summary>
        /// <param name="customer">
        /// Customer.
        /// </param>
        void UpdateBook(Book book);

        #endregion
    }
}