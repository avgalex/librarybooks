namespace LibraryBooks.DataObjects.SqlServer
{
    using LibraryBooks.DataObjects;

    /// <summary>
    /// Sql Server specific factory that creates Sql Server specific data access objects.
    /// </summary>
    /// <remarks>
    /// GoF Design Pattern: Factory.
    /// </remarks>
    public class SqlServerDaoFactory : IDaoFactory
    {
        /// <summary>
        /// Gets a Sql server specific customer data access object.
        /// </summary>
        public IBookDao BookDao
        {
            get { return new SqlServerBookDao(); } 
            
        }
    }
}
