namespace LibraryBooks.DataObjects.SqlServer
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;

    using BusinessObjects;

    using LibraryBooks.DataObjects;

    /// <summary>
    /// Sql Server specific data access object that handles data access of customers.
    /// </summary>
    public class SqlServerBookDao : IBookDao
    {

        public List<Book> SearchBooks(string isbn, string title, DateTime? datePublish, DateTime? datePurchase, string Title, string author, int pageNumber, string sortExpression)
        {
            string sql =
                @"SELECT [ISBN]
      ,[Title]
      ,[DatePublish]
      ,[DatePurchase]
      ,[Author]
      ,[PageNumber]
  FROM [dbo].[Books] ";

            var where = new StringBuilder();
            if (!string.IsNullOrEmpty(isbn))
            {
                where.Append(" WHERE ISBN = @Isbn");
            }

            if (!string.IsNullOrEmpty(title))
            {
                where.Append(where.Length == 0 ? " WHERE " : " AND ");
                where.Append(" [Title] LIKE @Title ");
            }

            if (datePublish.HasValue)
            {
                where.Append(where.Length == 0 ? " WHERE " : " AND ");
                where.Append(" [DatePublish] = @DatePublish");
            }

            if (datePurchase.HasValue)
            {
                where.Append(where.Length == 0 ? " WHERE " : " AND ");
                where.Append(" [DatePurchase] = @DatePurchase");
            }

            if (!string.IsNullOrEmpty(author))
            {
                where.Append(where.Length == 0 ? " WHERE " : " AND ");
                where.Append(" [Author] LIKE @Author ");
            }

            if (pageNumber > 0)
            {
                where.Append(where.Length == 0 ? " WHERE " : " AND ");
                where.Append(" [PageNumber] = @PageNumber ");
            }

            sql += where.ToString().OrderBy(sortExpression);

            object[] parms = { "@Isbn", isbn, "@Title", "%" + title + "%", "@DatePublish", datePublish, "@DatePurchase", datePurchase, "@Author", "%" + author + "%", "@PageNumber", pageNumber };
            

            List<Book> searchBooks = Db.ReadList(sql, Make, parms);

            return searchBooks;
        }

        /// <summary>
        /// Gets a sorted list of all customers.
        /// </summary>
        /// <param name="sortExpression">Sort order.</param>
        /// <returns>Sorted list of customers.</returns>
        public List<Book> GetBooks(string sortExpression)
        {
            string sql =
             @"SELECT [ISBN]
      ,[Title]
      ,[DatePublish]
      ,[DatePurchase]
      ,[Author]
      ,[PageNumber]
  FROM [dbo].[Books] ".OrderBy(sortExpression);

            return Db.ReadList(sql, Make);
        }

      

        /// <summary>
        /// Gets a customer.
        /// </summary>
        /// <param name="customerId">Unique customer identifier.</param>
        /// <returns>Customer.</returns>
        public Book GetBook(string Isbn)
        {
            string sql =
             @"SELECT [ISBN]
      ,[Title]
      ,[DatePublish]
      ,[DatePurchase]
      ,[Author]
      ,[PageNumber]
  FROM [dbo].[Books]
                WHERE [ISBN] = @Isbn";

            object[] parms = { "@Isbn", Isbn };
            return Db.Read(sql, Make, parms);
        }

        public void InsertBook(Book book)
        {
            string sql = @"INSERT INTO [dbo].[Books]
           ([ISBN]
           ,[Title]
           ,[DatePublish]
           ,[DatePurchase]
           ,[Author]
           ,[PageNumber]) 			
              VALUES (@Isbn, @Title, @DatePublish, @DatePurchase,
           @Author, @PageNumber)";

           Db.Insert(sql, this.Take(book));
        }

        /// <summary>
        /// Updates a customer.
        /// </summary>
        /// <param name="customer">Customer.</param>
        /// <returns>Number of customer records updated.</returns>
        public void UpdateBook(Book book)
        {
            string sql = @"UPDATE [dbo].[Books] SET 
[Title] = @Title
      ,[DatePublish] = @DatePublish
      ,[DatePurchase] = @DatePurchase
      ,[Author] = @Author
      ,[PageNumber] = @PageNumber
 WHERE [ISBN] = @Isbn";

            Db.Update(sql, this.Take(book));
        }

        /// <summary>
        /// Deletes a customer.
        /// </summary>
        /// <param name="customer">Customer.</param>
        /// <returns>Number of customer records deleted.</returns>
        public void DeleteBook(Book book)
        {
            string sql =
            @"DELETE FROM [dbo].[Books]
               WHERE [ISBN] = @Isbn";

            Db.Update(sql, this.Take(book));
        }

        private static Func<IDataReader, Book> Make = reader =>
           new Book() 
           {
               ISBN = reader["ISBN"].AsString(),
               Title = reader["Title"].AsString(),
               DatePublish = reader["DatePublish"].AsDateTime(),
               DatePurchase = reader["DatePurchase"].AsDateTime(),
               Author = reader["Author"].AsString(),
               PageNumber = reader["PageNumber"].AsInt(),
           };

        /// <summary>
        /// Creates query parameters list from Customer object
        /// </summary>
        /// <param name="customer">Customer.</param>
        /// <returns>Name value parameter list.</returns>
        private object[] Take(Book book)
        {
            return new object[]  
            {
                "@Isbn", book.ISBN,
                "@Title", book.Title,
                "@DatePublish", book.DatePublish,
                "@DatePurchase", book.DatePurchase,
                "@Author", book.Author,
                "@PageNumber", book.PageNumber
            };
        }
    }
}
