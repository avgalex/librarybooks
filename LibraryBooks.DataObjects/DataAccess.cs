namespace LibraryBooks.DataObjects
{
    using System.Configuration;

    public static class DataAccess
    {
        #region Static Fields

        /// <summary>
        /// The connection string name.
        /// </summary>
        private static readonly string connectionStringName =
            ConfigurationManager.AppSettings.Get("ConnectionStringName");

        /// <summary>
        /// The factory.
        /// </summary>
        private static readonly IDaoFactory factory = DaoFactories.GetFactory(connectionStringName);

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets the book dao.
        /// </summary>
        public static IBookDao BookDao
        {
            get
            {
                return factory.BookDao;
            }
        }

        #endregion
    }
}