namespace LibraryBooks.DataObjects
{
    /// <summary>
    /// Abstract factory interface. Creates data access objects.
    /// </summary>
    /// <remarks>
    /// GoF Design Pattern: Factory.
    /// </remarks>
    public interface IDaoFactory
    {
        /// <summary>
        /// Gets a customer data access object.
        /// </summary>
        IBookDao BookDao { get; }
    }
}
