﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FormBook.cs" company="">
//   
// </copyright>
// <summary>
//   L
//   The form book.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LibraryBooks.WindowsFormsClient
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;

    using LibraryBooks.Model.BusinessObjects;
    using LibraryBooks.WindowsFormsClient.Presenters;
    using LibraryBooks.WindowsFormsClient.Views;

    /// <summary>
    /// The form book.
    /// </summary>
    public partial class FormBook : Form, IBookView
    {
        #region Fields

        /// <summary>
        /// The book presenter.
        /// </summary>
        private BookPresenter bookPresenter;

        /// <summary>
        /// The cancel close.
        /// </summary>
        private bool cancelClose;

        /// <summary>
        /// The new book.
        /// </summary>
        private bool newBook;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FormBook"/> class.
        /// </summary>
        public FormBook()
        {
            this.InitializeComponent();

            this.Closing += this.FormBookClosing;

            // Initialize Presenter.
            this.bookPresenter = new BookPresenter(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FormBook"/> class.
        /// </summary>
        /// <param name="handler">
        /// The handler.
        /// </param>
        public FormBook(Action<IList<BookModel>> handler)
            : this()
        {
            this.bookPresenter.DisplaySearch = handler;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        public string Author
        {
            get
            {
                return this.txtAuthor.Text;
            }

            set
            {
                this.txtAuthor.Text = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether author search.
        /// </summary>
        public bool AuthorSearch
        {
            get
            {
                return this.chkAuthor.Checked;
            }
        }

        /// <summary>
        /// Gets or sets the date publish.
        /// </summary>
        public DateTime? DatePublish
        {
            get
            {
                return this.datePickerPublish.Value;
            }

            set
            {
                DateTime? d = value;
                if (d.HasValue)
                {
                    this.datePickerPublish.Value = d.Value;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether date publish search.
        /// </summary>
        public bool DatePublishSearch
        {
            get
            {
                return this.chkDatePublish.Checked;
            }
        }

        /// <summary>
        /// Gets or sets the date purchase.
        /// </summary>
        public DateTime? DatePurchase
        {
            get
            {
                return this.datePickerPurchase.Value;
            }

            set
            {
                var d = value;
                if (d.HasValue)
                {
                    this.datePickerPurchase.Value = value.Value;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether date purchase search.
        /// </summary>
        public bool DatePurchaseSearch
        {
            get
            {
                return this.chkDatePurchase.Checked;
            }
        }

        /// <summary>
        /// Gets or sets the isbn.
        /// </summary>
        public string ISBN
        {
            get
            {
                return this.txtIsbn.Text;
            }

            set
            {
                this.txtIsbn.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is search.
        /// </summary>
        public bool IsSearch { get; set; }

        /// <summary>
        /// Gets or sets the isbn.
        /// </summary>
        public string Isbn { get; set; }

        /// <summary>
        /// Gets a value indicating whether isbn search.
        /// </summary>
        public bool IsbnSearch
        {
            get
            {
                return this.chkISBN.Checked;
            }
        }

        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        public int PageNumber
        {
            get
            {
                return Convert.ToInt32(this.numericNumPage.Value);
            }

            set
            {
                decimal p = Convert.ToDecimal(value);
                this.numericNumPage.Value = p;
            }
        }

        /// <summary>
        /// Gets a value indicating whether page number search.
        /// </summary>
        public bool PageNumberSearch
        {
            get
            {
                return this.chkPageNumber.Checked;
            }
        }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title
        {
            get
            {
                return this.txtTitle.Text;
            }

            set
            {
                this.txtTitle.Text = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether title search.
        /// </summary>
        public bool TitleSearch
        {
            get
            {
                return this.chkTitle.Checked;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The create tooltip.
        /// </summary>
        private void CreateTooltip()
        {
            ToolTip toolTip1 = new ToolTip
                                   {
                                       AutoPopDelay = 5000, 
                                       InitialDelay = 500, 
                                       ReshowDelay = 500, 
                                       ShowAlways = true
                                   };

            string s =
                "Validates both ISBN 10 and ISBN 13 numbers, and confirms ISBN 13 numbers start with only 978 or 979.";
            toolTip1.SetToolTip(this.label1, s);
            toolTip1.SetToolTip(this.txtIsbn, s);
        }

        /// <summary>
        /// The form book closing.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FormBookClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = this.cancelClose;
            this.cancelClose = false;
        }

        /// <summary>
        /// The form book load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void FormBookLoad(object sender, EventArgs e)
        {
            this.CreateTooltip();

            if (this.IsSearch)
            {
                this.SetSearchForm();
            }
            else
            {
                this.SetEditForm();
            }
        }

        /// <summary>
        /// The set edit form.
        /// </summary>
        private void SetEditForm()
        {
            if (string.IsNullOrEmpty(this.Isbn))
            {
                this.Text = "New book";
                this.newBook = true;
            }
            else
            {
                this.Text = "Edit book";
                this.newBook = false;
            }

            this.bookPresenter.Display(this.Isbn);
        }

        /// <summary>
        /// The set search form.
        /// </summary>
        private void SetSearchForm()
        {
            this.Text = "Search book";
            this.newBook = false;
            this.buttonSave.Text = "Search";

            foreach (Control control in this.Controls)
            {
                if (control is CheckBox)
                {
                    control.Visible = true;
                    var chk = control as CheckBox;
                    chk.CheckState = CheckState.Checked;
                    chk.CheckState = CheckState.Unchecked;
                }
            }
        }

        /// <summary>
        /// The button save_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (this.IsSearch)
            {
                this.bookPresenter.SearchCriteria();
                this.Close();
            }
            else
            {
                if (string.IsNullOrEmpty(this.Author) || string.IsNullOrEmpty(this.ISBN)
                    || string.IsNullOrEmpty(this.Title) || this.PageNumber <= 0)
                {
                    // Do not close the dialog 
                    MessageBox.Show("All fields are required");
                    return;
                }

                try
                {
                    if (this.newBook)
                    {
                        this.bookPresenter.Create();
                    }
                    else
                    {
                        this.bookPresenter.Save();
                    }

                    this.Close();
                }
                catch (ApplicationException ex)
                {
                    MessageBox.Show(ex.Message, "Save failed");
                    this.cancelClose = true;
                }
            }
        }

        /// <summary>
        /// The chk author_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void chkAuthor_CheckedChanged(object sender, EventArgs e)
        {
            var chk = sender as CheckBox;

            this.txtAuthor.Enabled = chk.Checked;
        }

        /// <summary>
        /// The chk date publish_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void chkDatePublish_CheckedChanged(object sender, EventArgs e)
        {
            var chk = sender as CheckBox;

            this.datePickerPublish.Enabled = chk.Checked;
        }

        /// <summary>
        /// The chk date purchase_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void chkDatePurchase_CheckedChanged(object sender, EventArgs e)
        {
            var chk = sender as CheckBox;

            this.datePickerPurchase.Enabled = chk.Checked;
        }

        /// <summary>
        /// The chk isb n_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void chkISBN_CheckedChanged(object sender, EventArgs e)
        {
            var chk = sender as CheckBox;

            this.txtIsbn.Enabled = chk.Checked;
        }

        /// <summary>
        /// The chk page number_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void chkPageNumber_CheckedChanged(object sender, EventArgs e)
        {
            var chk = sender as CheckBox;

            this.numericNumPage.Enabled = chk.Checked;
        }

        /// <summary>
        /// The chk title_ checked changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void chkTitle_CheckedChanged(object sender, EventArgs e)
        {
            var chk = sender as CheckBox;

            this.txtTitle.Enabled = chk.Checked;
        }

        /// <summary>
        /// The text box 1_ leave.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void textBox1_Leave(object sender, EventArgs e)
        {
            string pattern = @"^(97(8|9))?\d{9}(\d|X)$";
            string input = this.txtIsbn.Text;

            if (!Regex.IsMatch(input, pattern))
            {
                MessageBox.Show("Invalid input", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        #endregion
    }
}