﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainForm.cs" company="">
//   
// </copyright>
// <summary>
//   The main form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace LibraryBooks.WindowsFormsClient
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using LibraryBooks.Model.BusinessObjects;
    using LibraryBooks.WindowsFormsClient.Presenters;
    using LibraryBooks.WindowsFormsClient.Views;

    /// <summary>
    /// The main form.
    /// </summary>
    public partial class MainForm : Form, IBooksView
    {
        #region Fields

        /// <summary>
        /// The book presenter.
        /// </summary>
        private BooksPresenter booksPresenter;

        /// <summary>
        /// The current isbn.
        /// </summary>
        private string currentIsbn;

        /// <summary>
        /// The cuurent book.
        /// </summary>
        private BookModel cuurentBook;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        public MainForm()
        {
            this.InitializeComponent();
            this.booksPresenter = new BooksPresenter(this);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Sets the books.
        /// </summary>
        public IList<BookModel> Books
        {
            set
            {
                var books = value;
                this.BindBooks(books);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The bind books.
        /// </summary>
        /// <param name="books">
        /// The books.
        /// </param>
        private void BindBooks(IList<BookModel> books)
        {
            if (books == null)
            {
                return;
            }

            this.dgvBooks.DataSource = null;
            this.dgvBooks.DataSource = books;
            this.dgvBooks.AutoResizeColumns();
        }

        /// <summary>
        /// The exit tool strip menu item click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ExitToolStripMenuItemClick(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// The main form_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            this.booksPresenter.Display();
            if (this.dgvBooks.Rows.Count > 0)
            {
                this.dgvBooks.Rows[0].Selected = true;
            }

            this.WindowState = FormWindowState.Maximized;
        }

        /// <summary>
        /// The add new tool strip menu item 1_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void addNewToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (var form = new FormBook())
            {
                if (form.ShowDialog() == DialogResult.OK)
                {
                    this.booksPresenter.Display();
                }
            }
        }

        /// <summary>
        /// The clear search result tool strip menu item_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void clearSearchResultToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.booksPresenter.Display();
        }

        /// <summary>
        /// The delete tool strip menu item 1_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new BookPresenter(null).Delete(this.currentIsbn);
            this.booksPresenter.Display();
        }

        /// <summary>
        /// The dgv books_ selection changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void dgvBooks_SelectionChanged(object sender, EventArgs e)
        {
            if (this.dgvBooks.SelectedRows.Count == 0)
            {
                return;
            }

            var row = this.dgvBooks.CurrentRow;

            this.currentIsbn = row.Cells["ISBN"].Value.ToString();
        }

        /// <summary>
        /// The edit tool strip menu item 1_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void editToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            using (var form = new FormBook())
            {
                form.Isbn = this.currentIsbn;
                if (form.ShowDialog() == DialogResult.OK)
                {
                    this.booksPresenter.Display();
                }
            }
        }

        /// <summary>
        /// The search book tool strip menu item_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void searchBookToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var form = new FormBook(this.booksPresenter.DisplaySearch))
            {
                form.IsSearch = true;
                form.ShowDialog();
            }
        }

        #endregion
    }
}