﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BooksPresenter.cs" company="">
//   
// </copyright>
// <summary>
//   The books presenter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LibraryBooks.WindowsFormsClient.Presenters
{
    using System.Collections.Generic;

    using LibraryBooks.Model.BusinessObjects;
    using LibraryBooks.WindowsFormsClient.Views;

    /// <summary>
    /// The books presenter.
    /// </summary>
    public class BooksPresenter : Presenter<IBooksView>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BooksPresenter"/> class.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        public BooksPresenter(IBooksView view)
            : base(view)
        {
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The display.
        /// </summary>
        public void Display()
        {
            this.View.Books = Model.GetBooks("ISBN ASC");
        }

        /// <summary>
        /// The display search.
        /// </summary>
        /// <param name="obj">
        /// The obj.
        /// </param>
        public void DisplaySearch(IList<BookModel> obj)
        {
            if (obj != null)
            {
                this.View.Books = obj;
            }
        }

        #endregion
    }
}