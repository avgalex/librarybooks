﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BookPresenter.cs" company="">
//   
// </copyright>
// <summary>
//   The book presenter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace LibraryBooks.WindowsFormsClient.Presenters
{
    using System;
    using System.Collections.Generic;

    using LibraryBooks.Model.BusinessObjects;
    using LibraryBooks.WindowsFormsClient.Views;

    /// <summary>
    /// The book presenter.
    /// </summary>
    public class BookPresenter : Presenter<IBookView>
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BookPresenter"/> class.
        /// </summary>
        /// <param name="view">
        /// The view.
        /// </param>
        public BookPresenter(IBookView view)
            : base(view)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the display search.
        /// </summary>
        public Action<IList<BookModel>> DisplaySearch { get; set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The create.
        /// </summary>
        public void Create()
        {
            var book = new BookModel
                           {
                               Author = this.View.Author, 
                               Title = this.View.Title, 
                               ISBN = this.View.ISBN, 
                               DatePublish = this.View.DatePublish, 
                               DatePurchase = this.View.DatePurchase, 
                               PageNumber = this.View.PageNumber
                           };

            Model.AddBook(book);
        }

        /// <summary>
        /// The delete.
        /// </summary>
        /// <param name="currentIsbn">
        /// The current isbn.
        /// </param>
        public void Delete(string currentIsbn)
        {
            Model.DeleteBook(currentIsbn);
        }

        /// <summary>
        /// The display.
        /// </summary>
        /// <param name="isbn">
        /// The isbn.
        /// </param>
        public void Display(string isbn)
        {
            if (string.IsNullOrEmpty(isbn))
            {
                return;
            }

            var book = Model.GetBook(isbn);

            this.View.Author = book.Author;
            this.View.DatePublish = book.DatePublish;
            this.View.DatePurchase = book.DatePurchase;
            this.View.ISBN = book.ISBN;
            this.View.Title = book.Title;
            this.View.PageNumber = book.PageNumber;
        }

        /// <summary>
        /// The save.
        /// </summary>
        public void Save()
        {
            var book = new BookModel
                           {
                               Author = this.View.Author, 
                               Title = this.View.Title, 
                               ISBN = this.View.ISBN, 
                               DatePublish = this.View.DatePublish, 
                               DatePurchase = this.View.DatePurchase, 
                               PageNumber = this.View.PageNumber
                           };

            Model.UpdateBook(book);
        }

        /// <summary>
        /// The search criteria.
        /// </summary>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public void SearchCriteria()
        {
            var book = new BookModel();
            if (this.View.IsbnSearch)
            {
                book.ISBN = this.View.ISBN;
            }

            if (this.View.AuthorSearch)
            {
                book.Author = this.View.Author;
            }

            if (this.View.TitleSearch)
            {
                book.Title = this.View.Title;
            }

            if (this.View.DatePublishSearch)
            {
                book.DatePublish = this.View.DatePublish;
            }

            if (this.View.DatePurchaseSearch)
            {
                book.DatePurchase = this.View.DatePurchase;
            }

            if (this.View.PageNumberSearch)
            {
                book.PageNumber = this.View.PageNumber;
            }

            var result = Model.SearchBooks(book);

            this.DisplaySearch(result);
        }

        #endregion
    }
}