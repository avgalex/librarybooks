﻿namespace LibraryBooks.WindowsFormsClient
{
    partial class FormBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtIsbn = new System.Windows.Forms.TextBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.datePickerPublish = new System.Windows.Forms.DateTimePicker();
            this.datePickerPurchase = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.numericNumPage = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.chkISBN = new System.Windows.Forms.CheckBox();
            this.chkTitle = new System.Windows.Forms.CheckBox();
            this.chkAuthor = new System.Windows.Forms.CheckBox();
            this.chkDatePublish = new System.Windows.Forms.CheckBox();
            this.chkDatePurchase = new System.Windows.Forms.CheckBox();
            this.chkPageNumber = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericNumPage)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "ISBN:";
            // 
            // txtIsbn
            // 
            this.txtIsbn.Location = new System.Drawing.Point(199, 32);
            this.txtIsbn.Name = "txtIsbn";
            this.txtIsbn.Size = new System.Drawing.Size(586, 31);
            this.txtIsbn.TabIndex = 1;
            this.txtIsbn.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(199, 84);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(586, 31);
            this.txtTitle.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 26);
            this.label2.TabIndex = 2;
            this.label2.Text = "Title:";
            // 
            // txtAuthor
            // 
            this.txtAuthor.Location = new System.Drawing.Point(199, 140);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(586, 31);
            this.txtAuthor.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "Author:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 26);
            this.label5.TabIndex = 8;
            this.label5.Text = "Date publish:";
            // 
            // datePickerPublish
            // 
            this.datePickerPublish.Location = new System.Drawing.Point(201, 190);
            this.datePickerPublish.Name = "datePickerPublish";
            this.datePickerPublish.Size = new System.Drawing.Size(584, 31);
            this.datePickerPublish.TabIndex = 9;
            // 
            // datePickerPurchase
            // 
            this.datePickerPurchase.Location = new System.Drawing.Point(201, 241);
            this.datePickerPurchase.Name = "datePickerPurchase";
            this.datePickerPurchase.Size = new System.Drawing.Size(584, 31);
            this.datePickerPurchase.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(159, 26);
            this.label6.TabIndex = 10;
            this.label6.Text = "Date purchase:";
            // 
            // numericNumPage
            // 
            this.numericNumPage.Location = new System.Drawing.Point(201, 295);
            this.numericNumPage.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericNumPage.Name = "numericNumPage";
            this.numericNumPage.Size = new System.Drawing.Size(584, 31);
            this.numericNumPage.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 300);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(174, 26);
            this.label7.TabIndex = 13;
            this.label7.Text = "Number of page:";
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(459, 360);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(6);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(150, 44);
            this.buttonCancel.TabIndex = 15;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonSave
            // 
            this.buttonSave.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonSave.Location = new System.Drawing.Point(277, 360);
            this.buttonSave.Margin = new System.Windows.Forms.Padding(6);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(150, 44);
            this.buttonSave.TabIndex = 14;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // chkISBN
            // 
            this.chkISBN.AutoSize = true;
            this.chkISBN.Location = new System.Drawing.Point(806, 36);
            this.chkISBN.Name = "chkISBN";
            this.chkISBN.Size = new System.Drawing.Size(22, 21);
            this.chkISBN.TabIndex = 16;
            this.chkISBN.UseVisualStyleBackColor = true;
            this.chkISBN.Visible = false;
            this.chkISBN.CheckedChanged += new System.EventHandler(this.chkISBN_CheckedChanged);
            // 
            // chkTitle
            // 
            this.chkTitle.AutoSize = true;
            this.chkTitle.Location = new System.Drawing.Point(806, 90);
            this.chkTitle.Name = "chkTitle";
            this.chkTitle.Size = new System.Drawing.Size(22, 21);
            this.chkTitle.TabIndex = 17;
            this.chkTitle.UseVisualStyleBackColor = true;
            this.chkTitle.Visible = false;
            this.chkTitle.CheckedChanged += new System.EventHandler(this.chkTitle_CheckedChanged);
            // 
            // chkAuthor
            // 
            this.chkAuthor.AutoSize = true;
            this.chkAuthor.Location = new System.Drawing.Point(806, 146);
            this.chkAuthor.Name = "chkAuthor";
            this.chkAuthor.Size = new System.Drawing.Size(22, 21);
            this.chkAuthor.TabIndex = 18;
            this.chkAuthor.UseVisualStyleBackColor = true;
            this.chkAuthor.Visible = false;
            this.chkAuthor.CheckedChanged += new System.EventHandler(this.chkAuthor_CheckedChanged);
            // 
            // chkDatePublish
            // 
            this.chkDatePublish.AutoSize = true;
            this.chkDatePublish.Location = new System.Drawing.Point(806, 198);
            this.chkDatePublish.Name = "chkDatePublish";
            this.chkDatePublish.Size = new System.Drawing.Size(22, 21);
            this.chkDatePublish.TabIndex = 19;
            this.chkDatePublish.UseVisualStyleBackColor = true;
            this.chkDatePublish.Visible = false;
            this.chkDatePublish.CheckedChanged += new System.EventHandler(this.chkDatePublish_CheckedChanged);
            // 
            // chkDatePurchase
            // 
            this.chkDatePurchase.AutoSize = true;
            this.chkDatePurchase.Location = new System.Drawing.Point(806, 249);
            this.chkDatePurchase.Name = "chkDatePurchase";
            this.chkDatePurchase.Size = new System.Drawing.Size(22, 21);
            this.chkDatePurchase.TabIndex = 20;
            this.chkDatePurchase.UseVisualStyleBackColor = true;
            this.chkDatePurchase.Visible = false;
            this.chkDatePurchase.CheckedChanged += new System.EventHandler(this.chkDatePurchase_CheckedChanged);
            // 
            // chkPageNumber
            // 
            this.chkPageNumber.AutoSize = true;
            this.chkPageNumber.Location = new System.Drawing.Point(806, 300);
            this.chkPageNumber.Name = "chkPageNumber";
            this.chkPageNumber.Size = new System.Drawing.Size(22, 21);
            this.chkPageNumber.TabIndex = 21;
            this.chkPageNumber.UseVisualStyleBackColor = true;
            this.chkPageNumber.Visible = false;
            this.chkPageNumber.CheckedChanged += new System.EventHandler(this.chkPageNumber_CheckedChanged);
            // 
            // FormBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 456);
            this.Controls.Add(this.chkPageNumber);
            this.Controls.Add(this.chkDatePurchase);
            this.Controls.Add(this.chkDatePublish);
            this.Controls.Add(this.chkAuthor);
            this.Controls.Add(this.chkTitle);
            this.Controls.Add(this.chkISBN);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.numericNumPage);
            this.Controls.Add(this.datePickerPurchase);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.datePickerPublish);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAuthor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIsbn);
            this.Controls.Add(this.label1);
            this.Name = "FormBook";
            this.Text = "Book";
            this.Load += new System.EventHandler(this.FormBookLoad);
            ((System.ComponentModel.ISupportInitialize)(this.numericNumPage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIsbn;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAuthor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker datePickerPublish;
        private System.Windows.Forms.DateTimePicker datePickerPurchase;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericNumPage;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.CheckBox chkISBN;
        private System.Windows.Forms.CheckBox chkTitle;
        private System.Windows.Forms.CheckBox chkAuthor;
        private System.Windows.Forms.CheckBox chkDatePublish;
        private System.Windows.Forms.CheckBox chkDatePurchase;
        private System.Windows.Forms.CheckBox chkPageNumber;
    }
}