﻿namespace LibraryBooks.WindowsFormsClient.Views
{
    using System;

    /// <summary>
    /// The BookView interface.
    /// </summary>
    public interface IBookView : IView
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        string Author { get; set; }

        /// <summary>
        /// Gets or sets the date publish.
        /// </summary>
        DateTime? DatePublish { get; set; }

        /// <summary>
        /// Gets or sets the date purchase.
        /// </summary>
        DateTime? DatePurchase { get; set; }

        /// <summary>
        /// Gets or sets the isbn.
        /// </summary>
        string ISBN { get; set; }

        /// <summary>
        /// Gets or sets the page number.
        /// </summary>
        int PageNumber { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        string Title { get; set; }

        bool IsbnSearch { get; }

        bool TitleSearch { get; }

        bool AuthorSearch { get;}

        bool DatePublishSearch { get; }

        bool DatePurchaseSearch { get; }

        bool PageNumberSearch { get; }
        #endregion
    }
}