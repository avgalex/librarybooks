﻿namespace LibraryBooks.WindowsFormsClient.Views
{
    using System.Collections.Generic;

    using LibraryBooks.Model.BusinessObjects;

    /// <summary>
    /// The BooksView interface.
    /// </summary>
    public interface IBooksView : IView
    {
        #region Public Properties

        /// <summary>
        /// Sets the books.
        /// </summary>
        IList<BookModel> Books { set; }

        #endregion
    }
}