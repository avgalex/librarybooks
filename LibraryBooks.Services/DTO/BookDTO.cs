﻿namespace LibraryBooks.Services.DTO
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The books dto.
    /// </summary>
    [DataContract(Name = "Book", Namespace = "http://www.yourcompany.com/types/")]

   
    public class BookDTO

    {
        /// <summary>
        /// Gets or sets the isbn.
        /// </summary>
        [DataMember]
        public string ISBN { get; set; }

         [DataMember]
        public string Title { get; set; }

         [DataMember]
        public DateTime DatePublish { get; set; }

         [DataMember]
        public DateTime DatePurchase { get; set; }

         [DataMember]
        public int PageNumber { get; set; }

         [DataMember]
        public string Author { get; set; }
    }
}