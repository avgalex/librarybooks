﻿namespace LibraryBooks.Services.DTO
{
    using System.Collections.Generic;
    using System.Linq;

    using BusinessObjects;

    public static class Mapper
    {
        public static Book FromDataTransferObject(BookDTO b)
        {
            if (b == null)
            {
                return null;
            }

            return new Book()
            {
               ISBN = b.ISBN,
               Author = b.Author,
               DatePublish = b.DatePublish,
               DatePurchase = b.DatePurchase,
               PageNumber = b.PageNumber,
               Title = b.Title
            };
        }



        public static IList<BookDTO> ToDataTransferObjects(IEnumerable<Book> books)
        {
            if (books == null)
            {
                return null;
            }
            
            return books.Select(ToDataTransferObject).ToList();
        }

        internal static BookDTO ToDataTransferObject(Book book)
        {
            if (book == null)
            {
                return null;
            }

            return new BookDTO
            {
               ISBN = book.ISBN,
               Author = book.Author,
               DatePublish = book.DatePublish,
               DatePurchase = book.DatePurchase,
               PageNumber = book.PageNumber,
               Title = book.Title
            };
        }

        public static IList<Book> FromDataTransferObjects(IList<BookDTO> books)
        {
            if (books == null)
            {
                return null;
            }

            return books.Select(FromDataTransferObject).ToList();
        }
    }
}