﻿namespace LibraryBooks.Services.DTO
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Holds criteria for customer queries.
    /// </summary>
      [DataContract(Namespace = "http://www.yourcompany.com/types/")]
    public class BookCriteria : Criteria
    {
        /// <summary>
        /// Unique customer identifier.
        /// </summary>
        [DataMember]
        public string ISBN { get; set; }

           [DataMember]
        public string Title { get; set; }

        [DataMember]
        public DateTime? DatePublish { get; set; }

        [DataMember]
        public DateTime? DatePurchase { get; set; }

        [DataMember]
        public int PageNumber { get; set; }

        [DataMember]
        public string Author { get; set; }
    }
}
