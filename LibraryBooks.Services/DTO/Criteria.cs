﻿namespace LibraryBooks.Services.DTO
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Base class that holds criteria for queries.
    /// </summary>
      [DataContract(Namespace = "http://www.yourcompany.com/types/")]
    public class Criteria
    {
        /// <summary>
        /// Sort expression of the criteria.
        /// </summary>
        [DataMember]
        public string SortExpression { get; set; }
    }
}