﻿namespace LibraryBooks.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;

    using BusinessObjects;

    using LibraryBooks.DataObjects;
    using LibraryBooks.Services.DTO;
    using LibraryBooks.Services.Messages;

    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class LibraryBooksService :ILibraryBooksService
    {
        private IBookDao booksDao = DataAccess.BookDao;

        public BookResponse GetBooks(BookRequest request)
        {
            var response = new BookResponse(request.RequestId);
            
            var criteria = request.Criteria as BookCriteria;
            string sort = criteria.SortExpression;

            if (request.LoadOptions.Contains("Books"))
            {
                IEnumerable<Book> books = this.booksDao.GetBooks(sort);

                response.Books = books.Select(Mapper.ToDataTransferObject).ToList();
            }

            if (request.LoadOptions.Contains("Book"))
            {
                Book book = this.booksDao.GetBook(criteria.ISBN);

                response.Book = Mapper.ToDataTransferObject(book);
            }

            if (request.LoadOptions.Contains("Search"))
            {
                IEnumerable<Book> books = this.booksDao.SearchBooks(
                    criteria.ISBN,
                    criteria.Title,
                    criteria.DatePublish,
                    criteria.DatePurchase,
                    criteria.Title,
                    criteria.Author,
                    criteria.PageNumber,
                    criteria.SortExpression);

                response.Books = books.Select(Mapper.ToDataTransferObject).ToList();
            }

            return response;
        }

        public BookResponse SetBooks(BookRequest request)
        {
            var response = new BookResponse(request.RequestId);
            
            var book = Mapper.FromDataTransferObject(request.Book);
            
                if (request.Action == "Create")
                {
                   this.booksDao.InsertBook(book);
                    response.Book = Mapper.ToDataTransferObject(book);
                }
                else if (request.Action == "Update")
                {
                    this.booksDao.UpdateBook(book);
                    response.Book = Mapper.ToDataTransferObject(book);
                }
                else if (request.Action == "Delete")
                {
                    var criteria = request.Criteria;
                    var cust = this.booksDao.GetBook(criteria.ISBN);

                    try
                    {
                        this.booksDao.DeleteBook(cust);
                        response.RowsAffected = 1;
                    }
                    catch
                    {
                        response.RowsAffected = 0;
                    }
                }
            

            return response;
        }
    }
}