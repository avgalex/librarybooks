﻿namespace LibraryBooks.Services
{
    using System.ServiceModel;

    using LibraryBooks.Services.Messages;

     [ServiceContract(SessionMode = SessionMode.Required)]
    public interface ILibraryBooksService
    {
          [OperationContract]
        BookResponse GetBooks(BookRequest request);

         [OperationContract]
         BookResponse SetBooks(BookRequest request);
    }
}