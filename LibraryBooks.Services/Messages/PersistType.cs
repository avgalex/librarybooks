namespace LibraryBooks.Services.Messages
{
    using System.Runtime.Serialization;

      [DataContract(Namespace = "http://www.yourcompany.com/types/")]
    public enum PersistType
    {
        /// <summary>
        /// Insert record in database.
        /// </summary>
        [EnumMember]
        Insert = 1,

        /// <summary>
        /// Update record in database.
        /// </summary>
        [EnumMember]
        Update = 2,

        /// <summary>
        /// Delete record from database.
        /// </summary>
        [EnumMember]
        Delete = 3
    }
}
