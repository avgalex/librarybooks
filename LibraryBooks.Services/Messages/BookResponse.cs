namespace LibraryBooks.Services.Messages
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using LibraryBooks.Services.DTO;

    /// <summary>
    /// Represents a customer response message to client
    /// </summary>    
      [DataContract(Namespace = "http://www.yourcompany.com/types/")]
    public class BookResponse : ResponseBase
    {
        /// <summary>
        /// Default Constructor for CustomerResponse.
        /// </summary>
        public BookResponse() { }

        /// <summary>
        /// Overloaded Constructor for CustomerResponse. Sets CorrelationId.
        /// </summary>
        /// <param name="correlationId"></param>
        public BookResponse(string correlationId) : base(correlationId) { }

        /// <summary>
        /// List of customers. 
        /// </summary>
        [DataMember]
        public IList<BookDTO> Books;

        /// <summary>
        /// Single customer
        /// </summary>
        [DataMember]
        public BookDTO Book;
    }
}
