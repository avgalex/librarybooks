using System.Runtime.Serialization;

namespace LibraryBooks.Services.Messages
{
    using LibraryBooks.Services.DTO;

    /// <summary>
    /// Represents a customer request message from client.
    /// </summary>
      [DataContract(Namespace = "http://www.yourcompany.com/types/")]
    public class BookRequest : RequestBase
    {
        /// <summary>
        /// Selection criteria and sort order
        /// </summary>
        [DataMember]
        public BookCriteria Criteria;

        /// <summary>
        /// Customer object.
        /// </summary>
        [DataMember]
        public BookDTO Book;
    }
}
